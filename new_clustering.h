//
// Created by 倉澤　一詩 on 2019/01/01.
//

#ifndef NEW_CLUSTERING_NEW_CLUSTERING_H
#define NEW_CLUSTERING_NEW_CLUSTERING_H


#include <iostream>
#include <iomanip>
#include <random>
#include <fstream>
#include "defines.h"

//struct Sample3Df {
//    double_t x, y, z;
//};
//
//struct SampleRGB{
//    int64_t r, g, b;
//};
//
//struct BelongClusterInfo {
//    double_t min_dir;
//    int64_t is_belonged_cluster_num;
//};



class Clustering3D {

private:

    int64_t prot_num;      //クラスタ数
    int64_t real_num;      //任意の点


    Sample3Df *prot_sample;
    Sample3Df *real_sample;

public:

    Clustering3D();

    ~Clustering3D();

    void sampling();

    void k_means();


};


#endif //NEW_CLUSTERING_NEW_CLUSTERING_H
