//
// Created by 倉澤　一詩 on 2019/01/08.
//

#ifndef NEW_CLUSTERING_RGB_CONTROL_H
#define NEW_CLUSTERING_RGB_CONTROL_H

#include <iostream>
#include "defines.h"
#include <iomanip>
#include <random>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

class RGBControl{

private:

    int64_t prot_num;      //クラスタ数
    int64_t real_num;      //任意の色数

    SampleRGB* prot_sample;
    SampleRGB** real_sample;

    cv::Vec3b color1, color2;
    cv::Mat image1, image2;

    int width = image1.cols;
    int height = image1.rows;


    int64_t x, y;
    uchar r, g, b;


public:

    RGBControl(std::string temp);

    ~RGBControl();

    void sampling();

    void k_means();

};


#endif //NEW_CLUSTERING_RGB_CONTROL_H
