//
// Created by 倉澤　一詩 on 2019/01/08.
//

#ifndef NEW_CLUSTERING_DEFINES_H
#define NEW_CLUSTERING_DEFINES_H

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

struct Sample3Df{
    double x, y, z;
};

struct Sample3D{
    int64_t x, y, z;
};

struct SampleRGB{
    uchar r, g, b;
};

struct BelongClusterInfo{

    double min_dir;
    int64_t is_belonged_cluster_num;
};

struct RGBBelongClusterInfo{

    int64_t min_dir;
    int64_t is_belonged_cluster_num;
};


#endif //NEW_CLUSTERING_DEFINES_H
