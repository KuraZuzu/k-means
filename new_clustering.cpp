//
// Created by 倉澤　一詩 on 2019/01/01.
//

#include "new_clustering.h"


Clustering3D::Clustering3D() {
    sampling();
}

Clustering3D::~Clustering3D() {
    delete[] prot_sample;
    delete[] real_sample;
}


void Clustering3D::sampling() {


    std::cout << std::endl;
    std::cout << "クラスタ数を入力:";
    std::cin >> prot_num;

    std::cout << std::endl;
    std::cout << "任意の点の数を入力:";
    std::cin >> real_num;

    std::cout << std::endl;

    real_sample = new Sample3Df[real_num];
    prot_sample = new Sample3Df[prot_num];


    std::random_device rd;

    std::mt19937_64 mt(rd());

    for (int64_t i = 0; i < prot_num; ++i) {

        prot_sample[i].x = mt() / 100000000000000000.0;
        prot_sample[i].y = mt() / 100000000000000000.0;
        prot_sample[i].z = mt() / 100000000000000000.0;
    }

    for (int64_t i = 0; i < real_num; ++i) {

        real_sample[i].x = mt() / 100000000000000000.0;
        real_sample[i].y = mt() / 100000000000000000.0;
        real_sample[i].z = mt() / 100000000000000000.0;

    }

    std::cout << "[PROT]" <<std::endl;

    for (int64_t i = 0; i < prot_num; ++i) {
        std::cout <<  "X:" << std::left << std::setw(9) << prot_sample[i].x << "  Y:" << std::left << std::setw(9) << prot_sample[i].y << "  Z:" << std::left << std::setw(9) << prot_sample[i].z << "  No." << i + 1 << std::endl;
    }

    std::cout << std::endl;


    std::cout << "[REAL]" << std::endl;

    for (int64_t i = 0; i < real_num; ++i) {
        std::cout <<  "X:" << std::left << std::setw(9) << real_sample[i].x << "  Y:" << std::left << std::setw(9) << real_sample[i].y << "  Z:" << std::left << std::setw(9) << real_sample[i].z << "  No." << i + 1 << std::endl;
    }

    std::cout << std::endl;


}

void Clustering3D::k_means() {

    int64_t loop_num = 0;
    int64_t belong_amount = 0;
    int64_t belong_change_amount = 0;
    bool end_flag;

    double_t dir;

    BelongClusterInfo* real_info;
    real_info = new BelongClusterInfo[real_num];

    for (int64_t i = 0; i < real_num; i++){
        real_info[i].min_dir = 1.79769e+308;       //1.79769e+308 は double型の最大値
        real_info[i].is_belonged_cluster_num = i;
    }

    BelongClusterInfo* old_real_info;
    old_real_info = new BelongClusterInfo[real_num];

    for (int64_t i = 0; i < real_num; i++){
        old_real_info[i].min_dir = 1.79769e+308;       //1.79769e+308 は double型の最大値
        old_real_info[i].is_belonged_cluster_num = i;
    }

    Sample3Df* mean_vector;
    mean_vector = new Sample3Df[prot_num];        //次のクラスタリングのための平均ベクトル座標



////////////////////////////クラスタリング全体の基本ループ////////////////////////////////////////////////
    while(true){
////////////////////////////クラスタリングのサンプル平均ベクトルの決定///////////////////////

        if(loop_num != 0) {
            for (int64_t i = 0; i < prot_num; i++) {
                for (int64_t j = 0; j < real_num; j++) {

                    if (real_info[j].is_belonged_cluster_num == i) {  ///////ここ変
                        mean_vector[i].x += real_sample[j].x;
                        mean_vector[i].y += real_sample[j].y;
                        mean_vector[i].z += real_sample[j].z;
                        belong_amount++;
                    }
                }

                if (belong_amount) {
                    mean_vector[i].x = mean_vector[i].x / belong_amount;
                    mean_vector[i].y = mean_vector[i].y / belong_amount;
                    mean_vector[i].z = mean_vector[i].z / belong_amount;
                }
                belong_amount = 0;
            }


//////////////////////////ループの初回だけ実行される、平均ベクトルを無視///////////////////////////////////////////////

        } else {
            for (int64_t i = 0; i < prot_num; i++) {
                mean_vector[i].x = prot_sample[i].x;
                mean_vector[i].y = prot_sample[i].y;
                mean_vector[i].z = prot_sample[i].z;
            }
        }

//////////////////////////クラスタリングをする一連の流れ//////////////////////////////
        for (int64_t i = 0; i < real_num; i++) {

            for (int64_t j = 0; j < prot_num; j++) {
                dir = (mean_vector[j].x - real_sample[i].x) * (mean_vector[j].x - real_sample[i].x)
                      + (mean_vector[j].y - real_sample[i].y) * (mean_vector[j].y - real_sample[i].y)
                      + (mean_vector[j].z - real_sample[i].z) * (mean_vector[j].z - real_sample[i].z);

                if (dir < real_info[i].min_dir) {
                    real_info[i].min_dir = dir;
                    real_info[i].is_belonged_cluster_num = j;
                }

            }
        }

///////////////////////////終了設定////////////////////////////////////////////

        for (int64_t i = 0; i < real_num; i++) {

            if(real_info[i].is_belonged_cluster_num != old_real_info[i].is_belonged_cluster_num) {
                belong_change_amount++;
            }
        }

        if(belong_change_amount == 0) end_flag = true;
        else end_flag = false;

        if(end_flag == true) break;


        //////////////////次のクラスタ再割り当てが発生しているかを調べるための準備として、同じ構造の別変数に渡す
        for (int64_t i = 0; i < real_num; i++) {
            old_real_info[i].is_belonged_cluster_num = real_info[i].is_belonged_cluster_num;
        }

        loop_num++;
        belong_change_amount = 0;
    }

    std::cout << "再割り当て発生回数:" << loop_num << std::endl;
    for (int64_t i = 0; i < real_num; i++) {
        std::cout <<  "X:" << std::left << std::setw(9) << real_sample[i].x
                  << "  Y:" << std::left << std::setw(9) << real_sample[i].y
                  << "  Z:" << std::left << std::setw(9) << real_sample[i].z
                  << "  クラスタ番号:" << real_info[i].is_belonged_cluster_num + 1 << std::endl;
    }

    std::ofstream ofs("クラスタリング.csv");

    ofs << "X" << "," << "Y" << "," << "Z" << "," << "クラスタ番号" << std::endl;

    for (int64_t i = 0; i < real_num; i++) {

        ofs << real_sample[i].x << "," << real_sample[i].y << "," << real_sample[i].z << "," << real_info[i].is_belonged_cluster_num + 1 << std::endl;

    }


//
//
//    for (int64_t i = 0; i < real_num; i++) {
//
//        std::cout << "X:" << real_sample[i].x << ",  Y:" << real_sample[i].y << ",  Z:" << real_sample[i].z << ",  クラスタ番号:" << real_sample[i].element_num <<std::endl;
//
//    }

    ofs.close();

    delete[] real_info;
    delete[] old_real_info;
    delete[] mean_vector;
}


