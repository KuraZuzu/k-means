#include "rgb_control.h"


RGBControl::RGBControl(std::string temp) {
//    image1 = cv::imread("/Users/zuzukun/Desktop/obj.907_in_WoT.jpg", 1);
    image1 = cv::imread(temp, 1);
    if ( !image1.data ) {
        printf("No image data \n");
        exit(1);
    }

    width = image1.cols;
    height = image1.rows;
    image2 = cv::Mat(cv::Size(width, height), CV_8UC3);
    sampling();
}

RGBControl::~RGBControl() {

    delete[] prot_sample;

    for (int x = 0; x < width; x++) {
        delete[] real_sample[x];
    }
    delete[] real_sample;
}


void RGBControl::sampling() {


    std::cout << std::endl;
    std::cout << "色数を入力:";
    std::cin >> prot_num;
    std::cout << std::endl;

    prot_sample = new SampleRGB[prot_num];

    real_sample = new SampleRGB* [width];
    for (int64_t i = 0; i < width; i++) {
        real_sample[i] = new SampleRGB [height];
    }

    std::random_device rd;

    std::mt19937_64 mt(rd());

    for (int64_t i = 0; i < prot_num; ++i) {

        prot_sample[i].r = (uchar)(mt() % 256);
        prot_sample[i].g = (uchar)(mt() % 256);
        prot_sample[i].b = (uchar)(mt() % 256);
    }


    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {

            color1 = image1.at<cv::Vec3b>(y, x);

            //R G B 分解
            real_sample[x][y].r = color1[2];
            real_sample[x][y].g = color1[1];
            real_sample[x][y].b = color1[0];
        }

    }

//////////////////////////////////////// デバッグ用 //////////////////////////////////////////////////////////////////////

//    std::cout << "[PROT]" <<std::endl;

//    for (int64_t i = 0; i < prot_num; i++) {
//        std::cout <<  "r:" << std::left << std::setw(6) << prot_sample[i].r << "  g:" << std::left << std::setw(6) << prot_sample[i].g << "  b:" << std::left << std::setw(6) << prot_sample[i].b << "  No." << i + 1 << std::endl;
//    }

//    std::cout << "[IMAGE]" <<std::endl;
//
//    for (int64_t y = 0; y < height; y++) {
//        for (int64_t x = 0; x < width; x++) {
//
//            std::cout << "r:" << std::left << std::setw(6) << real_sample[x][y].r
//                << "  g:" << std::left << std::setw(6) << real_sample[x][y].g
//                << "  b:" << std::left << std::setw(6) << real_sample[x][y].b
//                << " pixel [" << x << "][" << y << "]" << std::endl;
//        }
//    }

    std::cout << std::endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

void RGBControl::k_means() {

    int64_t loop_num = 0;
    int64_t belong_amount = 0;
    int64_t belong_change_amount = 0;
    bool end_flag;

    int64_t dir;

    RGBBelongClusterInfo** real_info;

    real_info = new RGBBelongClusterInfo* [width];
    for (int64_t i = 0; i < width; i++) {
        real_info[i] = new RGBBelongClusterInfo [height];
    }

    unsigned int init_count = 0;
    for (int64_t y = 0; y < height; y++){
        for (int64_t x = 0; x < width; x++) {

            real_info[x][y].min_dir = 9223372036854775807;       //int64_tの最大値
            real_info[x][y].is_belonged_cluster_num = init_count;
            init_count++;
        }
    }
    init_count = 0;

    RGBBelongClusterInfo** old_real_info;

    old_real_info = new RGBBelongClusterInfo*[width];
    for (int64_t i = 0; i < width; i++) {
        old_real_info[i] = new RGBBelongClusterInfo[height];
    }

    for (int64_t y = 0; y < height; y++){
        for (int64_t x = 0; x < width; x++) {

            old_real_info[x][y].min_dir = 9223372036854775807;       //int64_t型の最大値
            old_real_info[x][y].is_belonged_cluster_num = init_count;
            init_count++;
        }
    }

    Sample3Df* mean_vector;
    mean_vector = new Sample3Df[prot_num];     //次のクラスタリングのための平均ベクトル座標



////////////////////////////クラスタリング全体の基本ループ////////////////////////////////////////////////
    while(true){
////////////////////////////クラスタリングのサンプル平均ベクトルの決定///////////////////////

        if(loop_num != 0) {
            for (int64_t i = 0; i < prot_num; i++) {

                for (int64_t y = 0; y < height; y++) {
                    for (int64_t x = 0; x < width; x++) {

                        if (real_info[x][y].is_belonged_cluster_num == i) {
                            mean_vector[i].x += (double)real_sample[x][y].r;
                            mean_vector[i].y += (double)real_sample[x][y].g;
                            mean_vector[i].z += (double)real_sample[x][y].b;
                            belong_amount++;
                        }
                    }

                }

                if (belong_amount) {
                    mean_vector[i].x = mean_vector[i].x / belong_amount;
                    mean_vector[i].y = mean_vector[i].y / belong_amount;
                    mean_vector[i].z = mean_vector[i].z / belong_amount;
                }

                belong_amount = 0;
            }


//////////////////////////ループの初回だけ実行される、平均ベクトルを無視///////////////////////////////////////////////

        } else {
            for (int64_t i = 0; i < prot_num; i++) {
                mean_vector[i].x = (double)prot_sample[i].r;
                mean_vector[i].y = (double)prot_sample[i].g;
                mean_vector[i].z = (double)prot_sample[i].b;
            }
        }

//////////////////////////クラスタリングをする一連の流れ//////////////////////////////
        for (int64_t y = 0; y < height; y++) {
            for (int64_t x = 0; x < width; x++) {

                for (int64_t j = 0; j < prot_num; j++) {

                    dir = (mean_vector[j].x - real_sample[x][y].r) * (mean_vector[j].x - real_sample[x][y].r)
                          + (mean_vector[j].y - real_sample[x][y].g) * (mean_vector[j].y - real_sample[x][y].g)
                          + (mean_vector[j].z - real_sample[x][y].b) * (mean_vector[j].z - real_sample[x][y].b);

                    if (dir < real_info[x][y].min_dir) {
                        real_info[x][y].min_dir = dir;
                        real_info[x][y].is_belonged_cluster_num = j;
                    }

                }

            }
        }

///////////////////////////終了設定////////////////////////////////////////////

        for (int64_t x = 0; y < height; y++) {
            for (int64_t y = 0; x < width; x++) {

                if (real_info[x][y].is_belonged_cluster_num != old_real_info[x][y].is_belonged_cluster_num) {
                    belong_change_amount++;
                }
            }
        }

        if(belong_change_amount == 0) end_flag = true;
        else end_flag = false;

        if(end_flag == true) break;


        //////////////////次のクラスタ再割り当てが発生しているかを調べるための準備として、同じ構造の別変数に渡す
        for (int64_t y = 0; y < height; y++) {
            for (int64_t x = 0; x < width; x++) {
                old_real_info[x][y].is_belonged_cluster_num = real_info[x][y].is_belonged_cluster_num;
            }
        }

        loop_num++;
        belong_change_amount = 0;
    }

    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {

            for (int i = 0; i < prot_num; i++) {
                if (real_info[x][y].is_belonged_cluster_num == i) {
                    r = prot_sample[i].r;
                    g = prot_sample[i].g;
                    b = prot_sample[i].b;
                    break;
                }
            }
            //RGB値 から ピクセル値(カラー)を生成
            color2 = cv::Vec3b(b, g, r);

            //ピクセル値(カラー)を設定
            image2.at<cv::Vec3b>(y, x) = color2;
        }
    }

    cv::namedWindow("original_image");
    cv::imshow("original_image", image1);

    cv::namedWindow("processed_image");
    cv::imshow("processed_image", image2);

    cv::waitKey(0);
    cv::destroyAllWindows();

//////////////////////////////////////////// デバッグ用 //////////////////////////////////////////////////////////////////

//    std::cout << "再割り当て発生回数:" << loop_num << std::endl;
//    for (int64_t y = 0; y < height; y++) {
//        for (int64_t x = 0; x < width; x++) {
//
//            std::cout << "r:" << real_sample[x][y].r
//                      << "  g:" << real_sample[x][y].g
//                      << "  b:" << real_sample[x][y].b
//                      << "  クラスタ番号:" << real_info[x][y].is_belonged_cluster_num + 1 << std::endl;
//        }
//        std::cout << std::endl;
//    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    delete[] real_info;
    delete[] old_real_info;
    delete[] mean_vector;
}


